module Logic
  module Relations
    def equal(left, right)
      [
        IdentityStore.new do
          unify(left, right)
        end
      ]
    end

    def not_equal(left, right)
      [
        IdentityStore.new do
          disjoin(left, right)
        end
      ]
    end

    def all(*stores_lists)
      prod = Util.product(stores_lists)
      prod.map { |combo| combo.unshift(IdentityStore.new).inject(&:combine) }
        .keep_if(&:valid)
    end

    def any(*stores_lists)
      stores_lists.flatten.keep_if(&:valid)
    end

    def implies(p_left, p_right, q_left, q_right)
      # P -> Q is equivalent to !P || Q
      any(not_equal(p_left, p_right), equal(q_left, q_right))
    end

    def member(value, collection)
      [
        IdentityStore.new do
          update_domain(value, collection)
        end
      ]
    end
    alias_method :in, :member
    alias_method :domain_is, :member

    def must_include(collection, value)
      Proc.new do
        collection = walk(collection)
        possibilities = collection.select { |member| domains[member].include?(value) }
        if possibilities.length == 1 && unifications[possibilities[0]] != value
          equal(possibilities[0], value)
        elsif possibilities.empty?
          invalidate
        end
      end
    end

    def has_unique_contents(collection)
      all(*collection.combination(2).map { |pair| not_equal(*pair) })
    end
  end

end
