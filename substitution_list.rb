module Logic
  class SubstitutionList < Hash
    def unify!(left, right)
      changes = unify?(left, right)
      changes ? self.update(changes) : changes
    end

    def update(*args)
      super(*args)
    end

    def temp(changes = nil)
      changes ? self.dup.update(changes) : self.dup
    end

    def consistent?(other)
      return true if other.empty?
      !other.keys.any? do |k|
        self.has_key?(k) && self[k] != other[k]
      end
    end
  end

  class TriangularSubstitutionList < SubstitutionList
    def walk(val)
      if val.kind_of?(Symbol) && self.has_key?(val)
        walk(self[val])
      else
        val
      end
    end

    def unify?(left, right)
      left = walk(left)
      right = walk(right)
      changes = TriangularSubstitutionList.new

      return changes if [left, right].all? {|v| v.kind_of?(Symbol)} && (left == right)

      if left.kind_of? Symbol
        changes[left] = right
      elsif right.kind_of? Symbol
        changes[right] = left
      else
        changes = left.class.unify?(left, right, self)
      end
      changes
    end
  end
end
